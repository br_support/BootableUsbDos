	Misc Utilities

Enables special features of the Provit 2000 , 4000 and 5000 controllers.

For further information see the readme (*.gr, *.uk) files.

CMOS.BIN can be used to reset BIOS password:
brcmos -w=cmos2.bin